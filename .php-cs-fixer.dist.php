<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('assets/external')
    ->exclude('src/Migrations')
    ->exclude('public')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PSR1' => true,
        '@PSR2' => true,
        '@Symfony' => true,
        'yoda_style' => ['equal'=>false,'identical'=>false,'less_and_greater'=>false],
        'array_syntax' => ['syntax' => 'short'],
        'no_useless_else' => true,
        'binary_operator_spaces' => ['default'=>'single_space','operators'=>['=>'=>'align_single_space_minimal','='=>'align_single_space_minimal']],
    ])
    ->setFinder($finder)
;
